from sqlalchemy.orm import Session

from . import models, schemas


def get_post(db: Session, post_id: int):
    return db.query(models.Post).filter(models.Post.id == post_id).first()


def get_posts(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Post).offset(skip).limit(limit).all()


def create_post(db: Session, post: schemas.PostCreate):
    db_post = models.Post(
        title=post.title, description=post.description)
    db.add(db_post)
    db.commit()
    db.refresh(db_post)
    return db_post


def update_post(db: Session, post: schemas.PostCreate, post_id: int):
    db_post = db.query(models.Post).get(post_id)
    if db_post:
        db_post.title = post.title
        db_post.description = post.description
        db.commit()
        db.refresh(db_post)
        return db_post
    return False


def delete_post(db: Session, post_id=int):
    db_post = db.query(models.Post).get(post_id)
    if db_post:
        db.delete(db_post)
        db.commit()
        return True
    return False
