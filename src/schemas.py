from typing import List, Optional

from pydantic import BaseModel


class Post(BaseModel):
    id: int
    title: str
    description: str

    class Config:
        orm_mode = True


class PostCreate(BaseModel):
    title: str
    description: str
